######################################
#     BEGIN PART SELECTING SHARE     #
######################################
#Share test 1271  283? 431, 508, 82, 85, 94, 94, 107, 826, 405, 406, 204, 403 || 136,283,611,644,694,727, 801, 829, 863, 866, 873, 881, 884, 894, 911 |1008, 1019, 1023, 1055, 1085, 1086, 1087, 1089, 1093, 1117, 1219
#290, 418, 751, 792, 843, 891, 1025 | 479, 481, 489, 492, 500, 1062 | 4, 63, 76, 347, 352, 367, 373, 405, 406, 479, 481
#27, 63, 64, 67, 76, 96, 109, 114, 228 | 275, 288, 324, 347, 352, 367, 373, 405, 406, 418 | 91, 95, 127, 136, 213, 275, 288, 324, 347, 352,  367, 373, 391, 398, 405
#48, 193, 406, 479, 481, 489, 492, 500, 515, 562, 595, 605, 617 | 764, 845, 858, 861, 861, 864, 882, 896, 905, 920, 926, 930, 939, 1056, 1062, 1066, 1133
$FullPath = ""
$ShareName = ""
[int[]] $Myarray = 97
foreach ($nshare in $Myarray)
{
do{
    $Path = 0;
    while ($Path -eq $null -or $Path -eq '' -or $Path -lt 0 -or $Path -gt 10000) {
	    [int]$Path = $nshare
    }

    #write-Host ([Math]::truncate($Path/50))
    #write-Host ($Path/50)
    #write-Host ((([Math]::truncate($Path/100))+1)*100)
    #Write-Host (((([Math]::truncate($Path/100))+1)*100)-$Path)
    #write-Host (([Math]::truncate($Path/50)) -ne ($Path/50))

    $start = ([Math]::truncate($Path/50))*50;
    $end = $start + 50;
    #if(([Math]::truncate($Path/50)) -ne ($Path/50)){
    #    #if((((([Math]::truncate($Path/100))+1)*100)-$Path) -lt 50){
    #    #    $start += 1
    #    #}
    #}else{
    #    $start -= 50;
    #    $end = $start + 50;
    #}

    if(([Math]::truncate($Path/50)) -eq ($Path/50)){
        $start -= 50;
        $end = $start + 50;
    }
    $start = $start +1

    #write-Host $start"-"$end

    $complement = ""
    if($Path -lt 10000){
        $complement = "0"
        if($Path -lt 1000){
            $complement = $complement+"0"
            if($Path -lt 100){
                $complement = $complement+"0"
                if($Path -lt 10){
                    $complement = $complement+"0"
                }
            }
        }
    }
    if ($start -lt 50)
    {
        $start = 0
    }
    $ShareName= "M"+$complement+$Path
    
    $FullPath ="\\bt0d0000.w2k.bouyguestelecom.fr\Partages\"+$start +"-"+$end+"\"+$ShareName

    Write-Host "Path"$FullPath

    if(Test-Path $FullPath){
        Write-Host "Le share $ShareName existe"
    }else{
        Write-Host "Le share n'existe pas"
    }
}while(!(Test-Path $FullPath))



########################################
#         BEGIN GROUPS CREATION        #
########################################
$GroupNameList = $ShareName+"_L"
$GroupNameRead = $ShareName+"_R"
$GroupNameWrite= $ShareName+"_W"
echo $ShareName >> C:\Sources\sharemigre.txt
$ADPath = "OU=Share,OU=Groupes,DC=bt0d0000,DC=w2k,DC=bouyguestelecom,DC=fr"

write-Host "Create group $GroupNameList..."
New-ADGroup -Name $GroupNameList -SamAccountName $GroupNameList -GroupCategory Security -GroupScope DomainLocal -DisplayName $GroupNameList -Path $ADPath -Description $GroupNameList
write-Host "Create group $GroupNameRead..."
New-ADGroup -Name $GroupNameRead -SamAccountName $GroupNameRead -GroupCategory Security -GroupScope DomainLocal -DisplayName $GroupNameRead -Path $ADPath -Description $GroupNameRead
write-Host "Create group $GroupNameWrite..."
New-ADGroup -Name $GroupNameWrite -SamAccountName $GroupNameWrite -GroupCategory Security -GroupScope DomainLocal -DisplayName $GroupNameWrite -Path $ADPath -Description $GroupNameWrite

Write-Host "Wait 20 seconds for AD replication."
for($i=19;$i -ge 0;$i--){
    Start-Sleep -s 1
    if($i -eq 0){
        Write-Host "Ignition."
    }else{
        Write-Host "$i"
    }
    
}

########################################
#       BEGIN GROUPS POSITIONNING      #
########################################

Write-Host "Backup the old ACL:"
(Get-Acl $FullPath).Access
#BACKUP OLD ACL
$OldAcl = (Get-Acl $FullPath).Access | Select-Object -property IdentityReference,FileSystemRights
#CHANGE OWNER
$CurrentACL = Get-Acl $FullPath
write-Host "change owner"
takeown.exe /f $FullPath /a /r /d Y
Write-Host "Flushing the old ACL."
#FLUSH OLD ACL
$Acl = Get-Acl $FullPath
$Acl.SetAccessRuleProtection($true,$true)
echo "flag rule protection"
echo $Acl
start-sleep -s 10
$Acl = Get-Acl $FullPath
echo $Acl
start-sleep -s 5
$Acl.Access | %{$Acl.RemoveAccessRuleAll($_)}
echo $Acl
Set-Acl $FullPath $Acl

Write-Host "Building the old ACL."
#BUILDING NEW ACL
$Acl = Get-Acl $FullPath
$ace = New-Object System.Security.AccessControl.FileSystemAccessRule("SYSTEM","FullControl",”ContainerInherit,ObjectInherit”,”None”,"Allow")
$Acl.SetAccessRule($ace)
$ace = New-Object System.Security.AccessControl.FileSystemAccessRule("ADM_SECURITE","FullControl",”ContainerInherit,ObjectInherit”,”None”,"Allow")
$Acl.SetAccessRule($ace)
$ace = New-Object System.Security.AccessControl.FileSystemAccessRule("ADM_SHARE","FullControl",”ContainerInherit,ObjectInherit”,”None”,"Allow")
$Acl.SetAccessRule($ace)
$ace = New-Object System.Security.AccessControl.FileSystemAccessRule($GroupNameWrite,"Modify",”ContainerInherit,ObjectInherit”,”None”,"Allow")
$Acl.SetAccessRule($ace)
$ace = New-Object System.Security.AccessControl.FileSystemAccessRule($GroupNameList,"ListDirectory",”ContainerInherit,ObjectInherit”,”None”,"Allow")
$Acl.SetAccessRule($ace)
$ace = New-Object System.Security.AccessControl.FileSystemAccessRule($GroupNameRead,"ReadAndExecute",”ContainerInherit,ObjectInherit”,”None”,"Allow")
$Acl.SetAccessRule($ace)
$Acl.SetAccessRuleProtection($true,$true)

Write-Host "Applying the new ACL on $FullPath :"
Write-Host $Acl
Set-Acl $FullPath $Acl

Write-Host "Applying the new ACL on $FullPath subfolders"
#FORCING APPLIANCE OF THE NEW ACL ON THE FOLDER TREE
$FullSubDirsPath = $FullPath+"\*"
icacls $FullSubDirsPath /reset /T /C

##
# BEGIN ADD R/W GROUPS TO LIST GROUPS
##
Write-Host "Adding $GroupNameRead as a member of $GroupNameList"
Add-ADGroupMember -Identity (Get-ADGroup -Identity $GroupNameList -Properties DistinguishedName) -Members (Get-ADGroup -Identity $GroupNameRead -Properties DistinguishedName)
Write-Host "Adding $GroupNameWrite as a member of $GroupNameList"
Add-ADGroupMember -Identity (Get-ADGroup -Identity $GroupNameList -Properties DistinguishedName) -Members (Get-ADGroup -Identity $GroupNameWrite -Properties DistinguishedName)

##
# BEGIN COPYING GROUP POPULATION
##
Write-Host "Copying old group population to the new groups."
foreach($entry in $OldAcl){
    if( $entry.IdentityReference.ToString().indexOf("\ADM_") -eq -1 -and
        $entry.IdentityReference.ToString().indexOf("Domain Users") -eq -1 -and
        $entry.IdentityReference.ToString().indexOf("Authenticated users") -eq -1 -and
        $entry.IdentityReference.ToString().indexOf("\Users") -eq -1){
        
        # ADDING USERS WITH MODIFY PERMISSIONS
        if($entry.FileSystemRights.ToString().indexOf("Modify") -ne -1){
            Write-Host "Copying population of "$entry.IdentityReference.toString().split('\')[1]
            $sourceGroup = $entry.IdentityReference.toString().split('\')[1]

            $targetGroup = Get-ADGroup -Identity $GroupNameWrite -Properties DistinguishedName
            $sourcePopulation = Get-ADGroupMember -Identity $sourceGroup -Recursive
            foreach($Member in $sourcePopulation){
                Write-Host "Add $Member to $targetGroup"
                Add-ADGroupMember -Identity $targetGroup -Members $Member.distinguishedName
            }
        }elseif($entry.FileSystemRights.ToString().indexOf("ReadAndExecute") -ne -1){
            # ADDING USERS WITH READ PERMISSIONS
            Write-Host "Copying population of "$entry.IdentityReference.toString().split('\')[1]
            $sourceGroup = $entry.IdentityReference.toString().split('\')[1]

            $targetGroup = Get-ADGroup -Identity $GroupNameRead -Properties DistinguishedName
            $sourcePopulation = Get-ADGroupMember -Identity $sourceGroup -Recursive
            foreach($Member in $sourcePopulation){
                Write-Host "Add $Member to $targetGroup"
                Add-ADGroupMember -Identity $targetGroup -Members $Member.distinguishedName
            }
        }
    }
}
}

Write-Host "Program ended. Bye bye. Bisous tendresse."