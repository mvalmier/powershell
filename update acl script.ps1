
######################################
#     BEGIN PART SELECTING SHARE     #
######################################
#Share test 1271  283? 431, 508, 82, 85, 94, 94, 107, 826, 405, 406, 204, 403 || 136,283,611,644,694,727, 801, 829, 863, 866, 873, 881, 884, 894, 911 |1008, 1019, 1023, 1055, 1085, 1086, 1087, 1089, 1093, 1117, 1219
#290, 418, 751, 792, 843, 891, 1025 | 479, 481, 489, 492, 500, 1062 | 4, 63, 76, 347, 352, 367, 373, 405, 406, 479, 481
#27, 63, 64, 67, 76, 96, 109, 114, 228 | 275, 288, 324, 347, 352, 367, 373, 405, 406, 418 | 91, 95, 127, 136, 213, 275, 288, 324, 347, 352,  367, 373, 391, 398, 405
#48, 193, 406, 479, 481, 489, 492, 500, 515, 562, 595, 605, 617 | 764, 845, 858, 861, 861, 864, 882, 896, 905, 920, 926, 930, 939, 1056, 1062, 1066, 1133
$FullPath = ""
$ShareName = ""
$M72list = get-content -path C:\Users\ADM_SN3_SECU\name.txt
#$M72list = "WBO30 (Refonte SI Partenaire B-O)"
foreach ($m72name in $M72list)
{
    $ShareName = $m72name
    $FullPath ="\\bt0d0000\partages\51-100\M00072\Travail\PRJ (Projets)\"+$ShareName
    Write-Host "Path"$FullPath

    if(!(Test-Path $FullPath)){
        
        Write-Host "Le share n'existe pas"
        echo $ShareName >> C:\Sources\errorshareexist.txt
      
    }else{
        Write-Host "Le share $ShareName existe"
        createrdir $ShareName
        createrdir $ShareName
        
    }
}
function createrdir
{
param(
$ShareName
)
if ($ShareName.Length -gt 14)
{
$newname = echo $ShareName.Substring(0,15)
}
else{
$newname = echo $ShareName
}

$namemodify = $newname -replace '[^\p{L}\p{Nd}]'
echo $namemodify

########################################
#         BEGIN GROUPS CREATION        #
########################################
$GroupNameRead = $namemodify+"_R"
$GroupNameWrite= $namemodify+"_W"
echo $ShareName >> C:\Sources\sharemigre.txt
$ADPath = "OU=Share,OU=Groupes,DC=bt0d0000,DC=w2k,DC=bouyguestelecom,DC=fr"


write-Host "Create group $GroupNameRead..."
New-ADGroup -Name $GroupNameRead -SamAccountName $GroupNameRead -GroupCategory Security -GroupScope DomainLocal -DisplayName $GroupNameRead -Path $ADPath -Description $GroupNameRead
write-Host "Create group $GroupNameWrite..."
New-ADGroup -Name $GroupNameWrite -SamAccountName $GroupNameWrite -GroupCategory Security -GroupScope DomainLocal -DisplayName $GroupNameWrite -Path $ADPath -Description $GroupNameWrite

Write-Host "Wait 20 seconds for AD replication."
for($i=17;$i -ge 0;$i--){
    Start-Sleep -s 1
    if($i -eq 17){
        Write-Host "Ignition."
    }else{
        Write-Host "$i"
    }
    
}

########################################
#       BEGIN GROUPS POSITIONNING      #
########################################
if($GroupNameWrite -eq "_W"){
exit}

write-Host "change owner"
takeown.exe /f $FullPath /a /r /d Y
Write-Host "Flushing the old ACL."
#FLUSH OLD ACL
$Acl = Get-Acl $FullPath
$Acl.SetAccessRuleProtection($true,$true)
echo "flag rule protection"
echo $Acl
start-sleep -s 10
$Acl = Get-Acl $FullPath
echo $Acl
start-sleep -s 5
$Acl.Access | %{$Acl.RemoveAccessRuleAll($_)}
echo $Acl
Set-Acl $FullPath $Acl


Write-Host "Flushing the old ACL."
#FLUSH OLD ACL
$Acl = Get-Acl $FullPath
$Acl.SetAccessRuleProtection($true,$true)
echo "flag rule protection"
echo $Acl
start-sleep -s 10
$Acl = Get-Acl $FullPath
echo $Acl
start-sleep -s 5
$Acl.Access | %{$Acl.RemoveAccessRuleAll($_)}
echo $Acl
Set-Acl $FullPath $Acl

Write-Host "Building the old ACL."
#BUILDING NEW ACL
$Acl = Get-Acl $FullPath
$ace = New-Object System.Security.AccessControl.FileSystemAccessRule("SYSTEM","FullControl",”ContainerInherit,ObjectInherit”,”None”,"Allow")
$Acl.SetAccessRule($ace)
$ace = New-Object System.Security.AccessControl.FileSystemAccessRule("ADM_SECURITE","FullControl",”ContainerInherit,ObjectInherit”,”None”,"Allow")
$Acl.SetAccessRule($ace)
$ace = New-Object System.Security.AccessControl.FileSystemAccessRule("ADM_SHARE","FullControl",”ContainerInherit,ObjectInherit”,”None”,"Allow")
$Acl.SetAccessRule($ace)
$ace = New-Object System.Security.AccessControl.FileSystemAccessRule($GroupNameWrite,"Modify",”ContainerInherit,ObjectInherit”,”None”,"Allow")
$Acl.SetAccessRule($ace)
$ace = New-Object System.Security.AccessControl.FileSystemAccessRule($GroupNameRead,"ReadAndExecute",”ContainerInherit,ObjectInherit”,”None”,"Allow")
$Acl.SetAccessRule($ace)
$Acl.SetAccessRuleProtection($true,$true)

Write-Host "Applying the new ACL on $FullPath :"
Write-Host $Acl
Set-Acl $FullPath $Acl

Write-Host "Applying the new ACL on $FullPath subfolders"
#FORCING APPLIANCE OF THE NEW ACL ON THE FOLDER TREE
$FullSubDirsPath = $FullPath+"\*"
icacls $FullSubDirsPath /reset /T /C

##
# BEGIN ADD R/W GROUPS TO LIST GROUPS
##
##
# BEGIN COPYING GROUP POPULATION
##
Write-Host "Copying old group population to the new groups."
$a = Import-Csv "C:\sources\report.csv" -Delimiter ","| Select AccessPath,LogonName | where {$_.AccessPath -like "*$ShareName"}
$b = echo $a | select LogonName 
echo $b
foreach($i in $b)
{
    write-host $i.LogonName
   Add-ADGroupMember -Identity $GroupNameWrite -Members $i.LogonName

}
}
Write-Host "Program ended. Bye bye. Bisous tendresse."